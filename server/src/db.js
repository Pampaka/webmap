const {Sequelize} = require("sequelize");

const sequelize = new Sequelize(process.env.DB_CONN);

module.exports = {
    sequelize
}