const {Router} = require("express");
const routeAddress = Router();
const {AddressController} = require("../controllers/addressController");
const addressController  = new AddressController();

routeAddress.get("/", addressController.getAll);

module.exports = {
    routeAddress
}