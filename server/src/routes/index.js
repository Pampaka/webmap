const {Router} = require("express");
const routes = Router();
const {routeAddress} = require("./addressRoutes");

routes.use("/address", routeAddress);

module.exports = {
    routes
}