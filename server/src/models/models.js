const {sequelize} = require("../db");
const {Sequelize} = require("sequelize");

const Address = sequelize.define("address", {
    id: {type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true, allowNull: false},
    name: {type: Sequelize.STRING, allowNull: false},
    lat: {type: Sequelize.INTEGER, allowNull: true},
    lon: {type: Sequelize.INTEGER, allowNull: true},
})

module.exports = {
    Address
}