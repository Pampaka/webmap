const {Address} = require("../models/models");

class AddressController {

    async getAll(req, res, next) {
        try {
            const addresses = await Address.findAll();
            if (addresses.length === 0) throw new Error("Адреса не найдены");
            res.json(addresses);
        } catch (e) {
            next(e);
        }
    }

}

module.exports = {AddressController}
