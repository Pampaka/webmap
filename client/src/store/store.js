import {configureStore} from "@reduxjs/toolkit";
import mapReducer from "./mapReducer";

export default configureStore({
    reducer: {
        mapReducer: mapReducer,
    }
})