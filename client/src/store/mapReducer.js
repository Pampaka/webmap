import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {searchAddress, searchAddresses, searchInfrastructure} from "../http/addressesAPI";

const getAddresses = createAsyncThunk(
    "addresses/getAddresses",
    async (address) => {
        try {
            return await searchAddresses(address);
        } catch (e) {
            return e;
        }
    }
)

const getAddress = createAsyncThunk(
    "addresses/getAddress",
    async ({lat, lng}) => {
        try {
            return await searchAddress(lat, lng);
        } catch (e) {
            return e;
        }
    }
)

const getInfrastructure = createAsyncThunk(
    "addresses/getInfrastructure",
    async (type) => {
        try {
            return await searchInfrastructure(type);
        } catch (e) {
            return e;
        }
    }
)

export const mapReducer = createSlice({
    name: "addresses",
    initialState: {
        addressesList: [],
        infrastructureList: [],
        listAddress: {
            address: null,
            position: null,
        },
        clickAddress: {
            address: null,
            position: null,
        },
        loading: false,
        timeout: 0,
        error: null,
    },
    reducers: {
        resetAddressesList: (state) => {
            state.addressesList = [];
        },
        setListAddress: (state, {payload}) => {
            state.listAddress.address = payload;
            state.listAddress.position = {lat: payload.lat, lon: payload.lon};
        },
        setClickAddress: (state, {payload}) => {
            state.clickAddress.position = {lat: payload.lat, lon: payload.lng};
        },
        resetClickAddress: (state) => {
            state.clickAddress.address = null;
            state.clickAddress.position = null;
        }
    },
    extraReducers: {
        [getAddresses.pending]: (state) => {
            state.addressesList = [];
            state.error = null;
            state.loading = true;
        },
        [getAddresses.fulfilled]: (state, {payload}) => {
            state.loading = false;
            state.addressesList = payload;
        },
        [getAddresses.rejected]: (state, {payload}) => {
            state.loading = false;
            state.error = payload;
        },

        [getAddress.fulfilled]: (state, {payload}) => {
            state.clickAddress.address = payload;
        },

        [getInfrastructure.pending]: (state) => {
            state.infrastructureList = [];
        },
        [getInfrastructure.fulfilled]: (state, {payload}) => {
            state.infrastructureList = payload;
            console.log(payload)
        },
    }
})

export const {
    resetAddressesList,
    setListAddress,
    setClickAddress,
    resetClickAddress,
} = mapReducer.actions;
export default mapReducer.reducer;
export { getAddresses, getAddress, getInfrastructure };