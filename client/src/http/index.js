import axios from "axios";

export const nominatim = axios.create({
    baseURL: 'https://nominatim.openstreetmap.org',
    headers: {'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7'}
});

export const dadata = axios.create({
    baseURL: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/party',
    headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Token 7498edcd732a96f6bc1ba21052b3bdcb893bd931"
    },
    mode: "cors",
});

export const host = axios.create({
    baseURL: 'https://localhost:5000/api',
});