import {dadata, nominatim} from "./index";

export const searchAddresses = async (address) => {
    const {data} = await nominatim.get("/search", {
        params: {
            q: address,
            format: "json",
            countrycodes: "ru",
        }
    })
    return data;
}

export const searchAddress = async (lat, lon) => {
    const {data} = await nominatim.get("/reverse", {
        params: {
            lat: lat,
            lon: lon,
            format: "json",
            countrycodes: "ru",
        }
    })
    return data;
}

export const searchInfrastructure = async (type) => {
    const {data} = await dadata.post("", {
        query: type,
        count: 20,
        language: "ru",
        status: ["ACTIVE"],
        locations: [{ "kladr_id": 57 }]
    })
    return data?.suggestions;
}