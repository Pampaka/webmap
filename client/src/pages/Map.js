import React from 'react';
import {Container} from "react-bootstrap";
import MapBar from "../components/Map/MapBar/MapBar";
import MapComponent from "../components/Map/MapComponent";

const Map = () => {
    return (
        <Container className={"page-container-100"}>
            <div className={"d-flex justify-content-between"}>
                <div className={"map-bar-container mt-2"}>
                    <MapBar/>
                </div>
                <div style={{width: "100%"}}>
                    <MapComponent/>
                </div>
            </div>
        </Container>
    );
};

export default Map;