import React from 'react';
import {Navbar, Container, Nav} from "react-bootstrap";

const Header = () => {
    return (
        <div>
            <Navbar>
                <Container className={"page-container-100"}>
                    <Navbar.Collapse>
                        <Nav className="me-auto">
                            <Nav.Link href="/">Главная</Nav.Link>
                            <Nav.Link href="/map">Карта</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            <hr className={"mb-0 mt-0"}/>
        </div>
    );
};

export default Header;