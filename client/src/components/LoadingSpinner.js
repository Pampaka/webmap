import React from 'react';
import {Spinner} from "react-bootstrap";

const LoadingSpinner = () => {
    return (
        <div className={"d-flex justify-content-center mt-3 mb-3"}>
            <Spinner animation={"border"} variant={"primary"}/>
        </div>
    );
};

export default LoadingSpinner;