import React from 'react';
import {RoutesApp} from "../utils/routes";
import {Routes, Route} from "react-router";

const AppRouter = () => {
    return (
        <Routes>
            {RoutesApp.map(route =>
                <Route key={route.path} path={route.path} element={route.element()}/>
            )}
        </Routes>
    );
};

export default AppRouter;