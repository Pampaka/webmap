import React from 'react';
import {Marker, Tooltip, useMapEvents} from "react-leaflet";
import {getAddress, setClickAddress, resetClickAddress} from "../../../store/mapReducer";
import {useDispatch, useSelector} from "react-redux";
import {iconClick} from "../MarkerIcons";

const ClickMarker = () => {
    const {clickAddress} = useSelector(state => state.mapReducer);
    const dispatch = useDispatch();

    useMapEvents({
        click(e) {
            if (clickAddress.position) {
                dispatch(resetClickAddress());
            } else {
                dispatch(getAddress(e.latlng));
                dispatch(setClickAddress({...e.latlng}));
            }
        }
    })

    return (clickAddress.position && clickAddress.address) ? (
        <Marker position={clickAddress.position} icon={iconClick}>
            <Tooltip maxWidth={10} direction="top" offset={[0, -40]} size={[20, 20]} opacity={1} permanent>
                <span style={{
                    width: "200px",
                    display: "block",
                    whiteSpace: "normal"
                }}>{clickAddress.address?.display_name}</span>
            </Tooltip>
        </Marker>
    ) : null;
};

export default ClickMarker;