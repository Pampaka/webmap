import React, {useEffect} from 'react';
import {Marker, Popup, useMap} from "react-leaflet";
import {useSelector} from "react-redux";
import {iconSearch} from "../MarkerIcons";

const ListMarker = () => {
    const { listAddress } = useSelector(state => state.mapReducer);

    const map = useMap();

    useEffect(() => {
        if (listAddress.position) {
            map.flyTo(listAddress.position, map.getZoom());
        }
    }, [listAddress.position, map])

    return listAddress.position ? (
        <Marker position={listAddress.position} icon={iconSearch}>
            {listAddress.address &&
                <Popup>{listAddress.address?.display_name}</Popup>
            }
        </Marker>
    ) : null;
};

export default ListMarker;