import L from "leaflet";

const iconSearch = new L.Icon({
    iconRetinaUrl: require("../../assets/marker-blue.png"),
    iconUrl: require("../../assets/marker-blue.png"),
    shadowUrl: require("leaflet/dist/images/marker-shadow.png"),
    iconSize: [30, 40],
    iconAnchor: [15, 40],
    popupAnchor: [0, -30],
});

const iconClick = new L.Icon({
    iconRetinaUrl: require("../../assets/marker-point-blue.png"),
    iconUrl: require("../../assets/marker-point-blue.png"),
    shadowUrl: null,
    iconSize: [10, 10],
    iconAnchor: [5, 10],
    tooltipAnchor: [0, 20],
});

export { iconSearch, iconClick };