import React from 'react';
import {MapContainer, TileLayer} from "react-leaflet";
import ListMarker from "./Markers/ListMarker";
import ClickMarker from "./Markers/ClickMarker";

const MapComponent = () => {
    const initialZoom = 12;
    const initialPosition = {lat: 52.968061, lon: 36.096478};

    return (
        <MapContainer zoom={initialZoom} center={initialPosition}>
            <TileLayer url={'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'}/>
            <ListMarker/>
            <ClickMarker/>
        </MapContainer>
    );
};

export default MapComponent;