import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const TabTitle = ({icon, active}) => {
    return (
        <div className={"p-2 pb-0 pt-0"}>
            <FontAwesomeIcon
                icon={icon}
                color={active ? "var(--general)" : "var(--inactive)"}
                fontSize={"1.2rem"}
            />
        </div>
    );
};

export default TabTitle;