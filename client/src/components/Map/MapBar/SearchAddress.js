import React, {useState} from 'react';
import {Button, FormControl, ListGroup} from "react-bootstrap";
import classNames from "classnames";
import {getAddresses, setListAddress} from "../../../store/mapReducer";
import LoadingSpinner from "../../LoadingSpinner";
import {useDispatch, useSelector} from "react-redux";

const SearchAddress = () => {
    const {
        addressesList,
        listAddress,
        loading,
        error
    } = useSelector(state => state.mapReducer);
    const dispatch = useDispatch();

    const [addressSearch, setAddressSearch] = useState("");

    const search = () => {
        dispatch(getAddresses(addressSearch.trim()));
    }

    return (
        <div className={"d-flex flex-column map-tab"}>
            <h3>Поиск адреса</h3>
            <FormControl
                className={"mb-2"}
                placeholder={"Введите адрес"}
                onChange={e => setAddressSearch(e.target.value)}
                onKeyUp={e => {e.key === "Enter" && search()}}
                value={addressSearch}
            />
            <Button
                className={"mb-2 button-general"}
                onClick={search}
            >
                Поиск
            </Button>
            <div className={"mb-2 map-address-list"}>
                <ListGroup>
                    {addressesList.map(el =>
                        <ListGroup.Item
                            key={el?.display_name}
                            className={classNames("map-address-item", {
                                "map-address-item_active": listAddress.address === el
                            })}
                            variant={`${listAddress.address === el ? "dark" : "none"}`}
                            onClick={() => {
                                dispatch(setListAddress(el));
                            }}
                        >
                            {el.display_name}
                        </ListGroup.Item>
                    )}
                </ListGroup>
            </div>
            {loading &&
                <LoadingSpinner/>
            }
            {error &&
                <span>
                    {`Возникла ошибка в поиске адресов: ` +
                    `${error?.status} ${error?.message && ", " + error?.message}`}
                </span>
            }
        </div>
    );
};

export default SearchAddress;