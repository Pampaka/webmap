import React, {useState} from 'react';
import {Tabs, Tab} from "react-bootstrap";
import SearchAddress from "./SearchAddress";
import {faMagnifyingGlass, faBuilding} from "@fortawesome/free-solid-svg-icons";
import TabTitle from "./TabTitle";
import FilterInfrastructure from "./FilterInfrastructure";

const MapBar = () => {
    const [tab, setTab] = useState("search");

    return (
        <div className={"map-bar"}>
            <Tabs
                activeKey={tab}
                onSelect={k => setTab(k)}
                className="mb-2"
            >
                <Tab
                    eventKey="search"
                    title={<TabTitle icon={faMagnifyingGlass} active={tab === "search"}/>}
                >
                    <SearchAddress/>
                </Tab>
                <Tab
                    eventKey="infrastructure"
                    title={<TabTitle icon={faBuilding} active={tab === "infrastructure"}/>}
                >
                    <div className={"d-flex flex-column map-tab"}>
                        <FilterInfrastructure/>
                    </div>
                </Tab>
            </Tabs>
        </div>
    );
};

export default MapBar;