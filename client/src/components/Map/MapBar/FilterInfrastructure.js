import React, {useState} from 'react';
import {FormCheck} from "react-bootstrap";
import {useDispatch} from "react-redux";
import {getInfrastructure} from "../../../store/mapReducer";

const FilterInfrastructure = () => {
    const [ type, setType ] = useState();
    const dispatch = useDispatch();
    const types = ["Школа", "Больница", "Банк", "Университет", "Сбербанк"];

    return (
        <div className={"d-flex flex-column map-tab"}>
            {/*<h3>Поиск адреса</h3>*/}
            {types.map((t) =>
                <div key={t} className={"d-flex align-content-center"}>
                    <FormCheck
                        checked={type === t}
                        onChange={() => {
                            setType(t);
                            dispatch(getInfrastructure(t));
                        }}
                    />
                    <span>{t}</span>
                </div>
            )}
        </div>
    );
};

export default FilterInfrastructure;