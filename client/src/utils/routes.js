import {MAIN_ROUTE, MAP_ROUTE} from "./paths";
import Map from "../pages/Map";
import Main from "../pages/Main";

export const RoutesApp = [
    {
        path: MAIN_ROUTE,
        element: Main
    },
    {
        path: MAP_ROUTE,
        element: Map
    }
]

